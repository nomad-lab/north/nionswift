# NOMAD Remote Tools Hub (north) - nionswift container
Let's you run containerized tools remotely. North is based on Jupyterhub and NOMAD.
NOMAD runs Jupyterhub as a separate service that provides GUI elements to connect
with Jupyterhub for launching and controlling North tools.

This project contain the docker image for the nionswift container that offers specific
software tools from the research field of electron microscopy in NOMAD using a webbrowser.

<!--## Project structure

- `docker` - All the docker files, scripts for creating/managing images, documentation-->

## Getting started
Clone the project

```sh
git clone git@gitlab.mpcdf.mpg.de:nomad-lab/north/nionswift.git
cd apmtools
```

<!--Get all sub-modules

```sh
git submodule update --init
```-->

## Build an image

```sh
docker build -t gitlab-registry.mpcdf.mpg.de/nomad-lab/north/nionswift .
```

<!--See the respective `README.md` of `docker/*` subdirectories.-->

## Maintainer in FAIRmat
Sherjeel Shabih, Markus Kühbach
